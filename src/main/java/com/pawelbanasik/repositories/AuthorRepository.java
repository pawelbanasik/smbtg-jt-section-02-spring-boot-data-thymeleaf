package com.pawelbanasik.repositories;

import org.springframework.data.repository.CrudRepository;

import com.pawelbanasik.domain.Author;

public interface AuthorRepository extends CrudRepository<Author, Long>{
}
