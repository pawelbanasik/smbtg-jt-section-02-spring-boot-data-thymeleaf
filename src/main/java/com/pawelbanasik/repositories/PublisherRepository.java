package com.pawelbanasik.repositories;

import org.springframework.data.repository.CrudRepository;

import com.pawelbanasik.domain.Publisher;

public interface PublisherRepository extends CrudRepository<Publisher, Long> {

}
