package com.pawelbanasik.repositories;

import org.springframework.data.repository.CrudRepository;

import com.pawelbanasik.domain.Book;

public interface BookRepository extends CrudRepository<Book, Long> {

}
