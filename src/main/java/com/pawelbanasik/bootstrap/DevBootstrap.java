package com.pawelbanasik.bootstrap;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.pawelbanasik.domain.Author;
import com.pawelbanasik.domain.Book;
import com.pawelbanasik.domain.Publisher;
import com.pawelbanasik.repositories.AuthorRepository;
import com.pawelbanasik.repositories.BookRepository;
import com.pawelbanasik.repositories.PublisherRepository;

// ten listener slucha zmian w apce
@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

	private AuthorRepository authorRepostiory;
	private BookRepository bookRepository;
	private PublisherRepository publisherRepository;
	
	// nie potrzeba anotacji autowired nawet - injection przez constructor
	public DevBootstrap(AuthorRepository authorRepostiory, BookRepository bookRepository,
			PublisherRepository publisherRepository) {
		this.authorRepostiory = authorRepostiory;
		this.bookRepository = bookRepository;
		this.publisherRepository = publisherRepository;
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		initData();
	}

	private void initData() {
		// Eric
		Author eric = new Author("Eric", "Evans");
		Publisher hc = new Publisher("Harper Collins", "Sesame Street");
		Book ddd = new Book("Domain Driven Desing", "1234", hc);
		eric.getBooks().add(ddd);
		ddd.getAuthors().add(eric);

		// musza byc w odpowiedniej kolejnosci - Bardzo wazne!!!
		publisherRepository.save(hc);
		authorRepostiory.save(eric);
		bookRepository.save(ddd);
		
		// Rod
		Author rod = new Author("Rod", "Johnson");
		Publisher worx = new Publisher("Worx", "Tarnowska Street");
		Book noEJB = new Book("J2EE Development without EJB", "23444", worx);
		rod.getBooks().add(noEJB);
		noEJB.getAuthors().add(rod);

		// musza byc w odpowiedniej kolejnosci - Bardzo wazne!!!
		publisherRepository.save(worx);
		authorRepostiory.save(rod);
		bookRepository.save(noEJB);
	}

}
